package model.vo;

public class VODaylyStatistic {
	//TODO
	private String fecha;
	
	private int cantidadAccidentes;
	
	private int cantidadInfracciones;
	
	private double FINEAMT;
	
	public VODaylyStatistic(String pFecha, int pCantidadAccidentes, int pCantidadInfracciones, double pFINEAMT )
	{
		fecha = pFecha;
		cantidadAccidentes = pCantidadAccidentes;
		cantidadInfracciones = pCantidadInfracciones;
		FINEAMT = pFINEAMT;
	}
	
	public String darFecha()
	{
		return fecha;
	}
	
	public int darCantidadAccidentes()
	{
		return cantidadAccidentes;
	}
	
	public int darCantidadInfracciones()
	{
		return cantidadAccidentes;
	}
	
	public double darFINEAMT()
	{
		return FINEAMT;
	}
}
