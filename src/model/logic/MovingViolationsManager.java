package model.logic;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;

import model.data_structures.Cola;
import model.data_structures.Pila;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import com.opencsv.CSVReader;

public class MovingViolationsManager {

	private Cola<VOMovingViolations> pCola = new Cola<>();
	private Pila<VOMovingViolations> pPila = new Pila<>();

	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub

		try {
			@SuppressWarnings("resource")
			CSVReader Csvr = new CSVReader(new FileReader(movingViolationsFile));
			String[] lector = new String[16];

			while((lector = Csvr.readNext()) != null)
			{
				lector = Csvr.readNext();

				VOMovingViolations infraccion = new VOMovingViolations(Integer.parseInt(lector[0]), lector[2], Integer.parseInt(lector[9]), lector[13], lector[12], lector[15], lector[14]);
				pCola.enqueue(infraccion);
				pPila.push(infraccion);
				System.out.println(lector[0]);	
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Cola<VODaylyStatistic> getDalyStatistics()
	{
		Cola<VOMovingViolations> copia = pCola;
		Cola<VODaylyStatistic> retornar = new Cola<>();


		int cantidadAccidentes;
		int cantidadInfracciones;
		String[] fecha;
		int valorMultas = 0;

		while(copia.size() != 0)
		{
			cantidadAccidentes = 0;
			cantidadInfracciones = 0;
			fecha = copia.peek().getTicketIssueDate().split("T");

			if(copia.isEmpty() == false && copia.peek().getTicketIssueDate().startsWith(fecha[0]))
			{
				while(copia.isEmpty() == false && copia.peek().getTicketIssueDate().startsWith(fecha[0]) )
				{
					cantidadAccidentes ++;
					cantidadInfracciones ++;
					valorMultas += copia.dequeue().getTotalPaid();
						
				}

				retornar.enqueue(new VODaylyStatistic(fecha[0], cantidadAccidentes, cantidadInfracciones, valorMultas));
			}

			else
			{
				fecha = copia.peek().getTicketIssueDate().split("T");
			}

		}

		return retornar;
	}
	public Pila<VOMovingViolations> nLastAccidents(int	n)
	{
		Pila<VOMovingViolations> retornar = new Pila<VOMovingViolations>();
		for(int i =0; i<n;i++)
		{
			VOMovingViolations nuevo=pPila.pop();
			retornar.push(nuevo);;
		}
		return retornar;
		
		
	}



}
